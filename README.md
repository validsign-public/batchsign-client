# ValidSign | BatchSign

With BatchSign, ValidSign introduces a solution to sign a large amount of documents.

This article is about:

[[_TOC_]]

## Workflow

This section describes how the workflow of the BatchSign client looks like.

- Get the certificate chain from BatchSign server
- Prepare the document
    - Load the document
    - Get a transaction id from BatchSign server
    - Add an empty signature field to the document
        - Within the reason code
        - Within the location code
    - Calculate the document SHA-256 hash
- Sign the document
    - Send document hash to BatchSign server
    - Receive the signed document hash
    - Put the signed hash into the signature
    - Check the signature
- Your are done

![BatchSign info graphic](documentation/images/info_graphic.png "Info graphic")


------

## Getting started - Client

Download the BatchSign client, if you want to use the BatchSign solution on your local Windows machine. The last version
is under the Download section available.

### System requirements

- Java 11

### ValidSign requirements

- Registered user with API key
- Supported document type
    - .pdf

### Features

- Install client
- Sign single document
- Sign multiple documents
- Change API key
- Change output folder

#### Install client

This section describes how to install the BatchSign client.

![](documentation/videos/set_api_key_output_folder.mp4)

- Download the latest version of the BatchSign client.
- Start the client.
- At this moment there are no API and output folder (for the signed documents) available.
- Go to **Edit** => **Preferences**
- Enter your API key
- Click **Edit**
- Select a output folder.
- Click **Select Folder**
- Click **Save**
- Restart the application

------

#### Sign single document

This section describes how to sign single document.

![](documentation/videos/sign_single_document.mp4)

- Click **Files**
- Select your document, you want to sign.
- Click **Open**
- The client shows that your file is prepared for the signing process.
- Click **Sign**
- The client shows that your document is signed and placed into the output folder.
- Your done.

------

#### Sign multiple documents

This section describes how to sign multiple documents.

![](documentation/videos/sign_multiple_documents.mp4)

- Click **Files**
- Select your documents, you want to sign.
- Click **Open**
- The client shows that your files are prepared for the signing process.
- Click **Sign**
- The client shows that your documents are signed and placed into the output folder.
- Your done.

------

#### Change your API key

This section describes how to change the API key.

- Go to **Edit** => **Preferences**
- Enter your API key
- Click **Save**

------

#### Change your output folder

This section describes how to change your output folder.

- Go to **Edit** => **Preferences**
- Click **Edit**
- Select a output folder.
- Click **Select Folder**
- Click **Save**

------

## Getting started - Your BatchSign integration

You are not bound on the client to use BatchSign solution. It's possible to build a own integration and use the
BatchSign solution in your environment. For more detail information about the BatchSign API have a look on
the [BatchSign API documentation](https://hsm.sandbox.validsign.nl/swagger-ui.html#).

We are using the [iText](https://itextpdf.com/) solution to prepare and sign the documents. iText serves an API for Java
as well for .Net to manipulate PDF documents. So you can use it in your own integration as well.

In the section below we explain what is necessary to do that.

- Get the certificate chain from BatchSign server
- Prepare the document
    - Load the document
    - Get a transaction id from BatchSign server
    - Add an empty signature field to the document
        - Within the reason code
        - Within the location code
    - Calculate the document SHA256 hash
- Sign the document
    - Send document hash to BatchSign server
    - Receive the signed document hash
    - Put the signed hash into the signature
    - Check the signature
- Your are done

#### Get a transaction id from BatchSign server

The first step of the BatchSign process is to get a transaction, within

##### Reason code

Contains the signer email address and signer id. An example looks like:

> E-SIGNED BY yourCompany@name.com | ID: 4efe1863-cf67-49ec-98fc-3d1e53ed5c74

##### Location code

Contains the signer IP address and the transaction id. An example looks like:

> IP: 108.62.152.76 | Transaction ID: Xasdjlasdjkaiosdkajlas/aad15avave545ega=

##### Transaction id

> Xasdjlasdjkaiosdkajlas/aad15avave545ega=

The transaction id is needed for the signing process.

Java code example:

```java
public static Transaction getTransactionId(int amount,String apiKey)throws UnirestException{
        Unirest.setTimeouts(0,0);
        HttpResponse<String> response=
        Unirest.get(GET_TRANSACTION_ID_URL+"?"+AMOUNT_PARAM+"="+amount)
        .header("Authorization",apiKey)
        .asString();
        if(response.getStatus()==HttpStatus.SC_OK){
        JSONObject jsonResponse=new JSONObject(response.getBody());
        String reason=jsonResponse.getString("reason");
        String location=jsonResponse.getString("location");
        String transactionId=jsonResponse.getString("transactionId");
        return new Transaction(reason,location,transactionId);
        }

        logger.error("Could not get a transaction id from BatchSign server.");
        return null;
        }
```

#### Add an empty signature field to the document

Before you can calculate the hash of a document, you have to include an empty signature field. This field contains the
reason code, location code and the public certificate. Furthermore the empty field needs a name, e.g.

> signatureField

During the sign process we are able to find the empty signature field by this name.

```java
public void createEmptySignField()throws IOException,GeneralSecurityException{
        try(PdfReader reader=new PdfReader(sourceFile);
        FileOutputStream tempFileOutputStream=new FileOutputStream(tempFile)){
        PdfSigner signer=
        new PdfSigner(reader,tempFileOutputStream,new StampingProperties());

        PdfSignatureAppearance appearance=signer.getSignatureAppearance();
        appearance
        .setReason(transaction.getReason())
        .setLocation(transaction.getLocation())
        .setPageNumber(1)
        .setCertificate(chain[0]);
        signer.setFieldName(FIELDNAME);

        /* ExternalBlankSignatureContainer constructor will create the PdfDictionary for the signature
         * information and will insert the /Filter and /SubFilter values into this dictionary.
         * It will leave just a blank placeholder for the signature that is to be inserted later.
         */
        IExternalSignatureContainer external=
        new ExternalBlankSignatureContainer(PdfName.Adobe_PPKLite,PdfName.Adbe_pkcs7_detached);

        // Sign the document using an external container.
        // 9800 is the size of the empty signature placeholder.
        signer.signExternalContainer(external,9800);
        }
        }
```

#### Calculate the document SHA-256 hash

The method below shows an example of calculating a SHA-256 document hash.

> Don't modify the document anymore, after you calculated the document hash. Otherwise the signature will be invalid.

```java
public String calculateDocumentHash()throws IOException,GeneralSecurityException{
        try(PdfReader pdfReader=new PdfReader(tempFile);
        PdfDocument document=new PdfDocument(pdfReader)){
        IRandomAccessSource readerSource=
        document.getReader().getSafeFile().createSourceView();
        SignatureUtil signatureUtil=new SignatureUtil(document);
        PdfSignature signature=signatureUtil.getSignature(FIELDNAME);
        PdfArray b=signature.getByteRange();
        long[]gaps=b.toLongArray();
        try(InputStream rg=
        new RASInputStream((new RandomAccessSourceFactory()).createRanged(readerSource,gaps))){

final String hashAlgorithm="SHA-256";
        BouncyCastleDigest digest=new BouncyCastleDigest();
        PdfPKCS7 pkcs7=new PdfPKCS7(null,chain,hashAlgorithm,null,digest,false);
        documentHash=DigestAlgorithms.digest(rg,digest.getMessageDigest(hashAlgorithm));
        authenticatedAttributeBytes=
        pkcs7.getAuthenticatedAttributeBytes(
        documentHash,PdfSigner.CryptoStandard.CMS,null,null);
        }
        }
```

#### Put the signed hash into the signature

After you send the document hash to the BatchSign server, you will receive a signed hash. With this signed hash you are
able to sign your document.

```java
public void createSignature()throws IOException{
        try(PdfReader reader=new PdfReader(new FileInputStream(tempFile));
        FileOutputStream destinationOutputStream=new FileOutputStream(destinationFile)){
        PdfSigner signer=
        new PdfSigner(reader,destinationOutputStream,new StampingProperties());

        IExternalSignatureContainer external=
        new PdfExternalSignatureContainer(chain,signedHash);

        signer.signDeferred(signer.getDocument(),FIELDNAME,destinationOutputStream,external);
        }
        tempFile.deleteOnExit();
        }
```

The method below is an example of our own PdfExternalSignatureContainer. This method sets the signed hash into signature
and checks if the signature is valid.

```java
@Override
public byte[]sign(InputStream inputStream)throws GeneralSecurityException{
        try{
        BouncyCastleDigest digest=new BouncyCastleDigest();
        PdfPKCS7 signer=new PdfPKCS7(null,chain,HASH_ALGORITHM,null,digest,false);

        byte documentHash[]=
        DigestAlgorithms.digest(inputStream,digest.getMessageDigest(HASH_ALGORITHM));

        byte[]authenticatedAttributeBytes=
        signer.getAuthenticatedAttributeBytes(
        documentHash,PdfSigner.CryptoStandard.CMS,null,null);

        signer.setExternalDigest(signedHash,null,"RSA");

        Signature signature=Signature.getInstance(SIGNATURE_INSTANCE);
        signature.initVerify(chain[0].getPublicKey());
        signature.update(authenticatedAttributeBytes);
        if(!signature.verify(signedHash)){
        throw new GeneralSecurityException("Signature is not correct.");
        }

        ITSAClient tsaClient=new TSAClientBouncyCastle(TSA_URL,null,null);

        return signer.getEncodedPKCS7(
        documentHash,PdfSigner.CryptoStandard.CMS,tsaClient,null,null);
        }catch(IOException e){
        e.printStackTrace();
        }
        return null;
        }
```

------

## Download

| Version |                         Application                          |
| :-----: | :----------------------------------------------------------: |
|  0.0.1  | [download](.\documentation\applications\0_0_1\BatchSign.exe) |
|  1.0.0  | [download](.\documentation\applications\1_0_0\BatchSign.exe) |

------

## Troubleshoot

------

## License

By following the rules of the Affero General Public License (AGPL), you may use the BatchSign client at no cost.
