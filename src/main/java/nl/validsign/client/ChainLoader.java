package nl.validsign.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

/** The public certificate chain loader. */
public class ChainLoader {

  /** The file for the public certificate */
  private final File publicCertificateFile;

  /** The public certificate chain */
  private final Certificate[] chain;

  /**
   * Constructor.
   *
   * @throws IOException If a IO error occurs
   * @throws NoSuchProviderException If a provider error occurs
   * @throws CertificateException If a certificate error occurs
   * @throws NoSuchAlgorithmException If an algorithm error occurs
   * @throws InvalidKeyException If a key error occurs
   */
  public ChainLoader(File publicCertificateFile)
      throws IOException, NoSuchProviderException, CertificateException, NoSuchAlgorithmException,
          InvalidKeyException {
    this.publicCertificateFile = publicCertificateFile;
    Certificate[] certs = loadCertificates();
      chain = loadChain((X509Certificate) certs[0], certs);
  }

  /**
   * Gets the public certificates chain.
   *
   * @return public certificate chain
   */
  public Certificate[] getChain() {
    return chain;
  }

  /**
   * Loads the chain of the given certificates.
   *
   * @param startingPoint The starting point of the chain.
   * @param certificates The certificates
   * @return certificate chain
   * @throws NoSuchAlgorithmException If a algorithm error occurs
   * @throws InvalidKeyException If a key error occurs
   * @throws NoSuchProviderException If a provider error occurs
   * @throws CertificateException If a certificate error occurs
   */
  private X509Certificate[] loadChain(
      X509Certificate startingPoint, Certificate[] certificates)
      throws NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException,
          CertificateException {
    return loadChain(startingPoint, new LinkedList(Arrays.asList(certificates)));
  }

  /**
   * Loads the chain of the given certificates.
   *
   * @param startingPoint The starting point of the first certificate
   * @param certificates The certificates
   * @return array of certificates
   * @throws NoSuchAlgorithmException If an algorithm error occurs
   * @throws InvalidKeyException If a key error occurs
   * @throws NoSuchProviderException If a provider error occurs
   * @throws CertificateException If a certificate error occurs
   */
  private X509Certificate[] loadChain(
      X509Certificate startingPoint, Collection certificates)
      throws NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException,
          CertificateException {

    LinkedList path = new LinkedList();
    path.add(startingPoint);
    boolean nodeAdded = true;
    while (nodeAdded) {
      nodeAdded = false;
      X509Certificate top = (X509Certificate) path.getLast();
      if (isSelfSigned(top)) {
        break;
      }

      Iterator it = certificates.iterator();
      while (it.hasNext()) {
        X509Certificate x509 = (X509Certificate) it.next();
        if (verify(top, x509.getPublicKey())) {
          path.add(x509);
          nodeAdded = true;
          it.remove();
          break;
        }
      }
    }
    X509Certificate[] results = new X509Certificate[path.size()];
    path.toArray(results);
    return results;
  }

  /**
   * Checks if certificate is self signed.
   *
   * @param cert The certificate
   * @return isSelfSigned
   * @throws CertificateException If a certificate error occurs
   * @throws InvalidKeyException If a key error occurs
   * @throws NoSuchAlgorithmException If an algorithm error occurs
   * @throws NoSuchProviderException If a provider error occurs
   */
  private boolean isSelfSigned(X509Certificate cert)
      throws CertificateException, InvalidKeyException, NoSuchAlgorithmException,
          NoSuchProviderException {

    return verify(cert, cert.getPublicKey());
  }

  /**
   * Verifies the certificate and the public key.
   *
   * @param cert The certificate
   * @param key The public key
   * @return verified
   * @throws CertificateException If a certificate error occurs
   * @throws InvalidKeyException If a key error occurs
   * @throws NoSuchAlgorithmException If an algorithm error occurs
   * @throws NoSuchProviderException If a provider error occurs
   */
  private boolean verify(X509Certificate cert, PublicKey key)
      throws CertificateException, InvalidKeyException, NoSuchAlgorithmException,
          NoSuchProviderException {

    String signatureAlgorithm = cert.getSigAlgName();
    String keyAlgorithm = key.getAlgorithm();
    signatureAlgorithm = signatureAlgorithm != null ? signatureAlgorithm.trim().toUpperCase() : "";
    keyAlgorithm = keyAlgorithm != null ? keyAlgorithm.trim().toUpperCase() : "";
    if (keyAlgorithm.length() >= 2 && signatureAlgorithm.endsWith(keyAlgorithm)) {
      try {
        cert.verify(key);
        return true;
      } catch (SignatureException se) {
        return false;
      }
    } else {
      return false;
    }
  }

  /**
   * Loads certificates.
   *
   * @return certificates
   * @throws IOException If a IO error occurs
   */
  private Certificate[] loadCertificates() throws IOException {
    Certificate[] certs = null;
    if (publicCertificateFile.canRead()) {
      try {
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        FileInputStream fileInputStream = new FileInputStream(publicCertificateFile);
        Collection<? extends Certificate> certificates =
            certificateFactory.generateCertificates(fileInputStream);
        if (certificates.size() > 0) {
          certs = new Certificate[certificates.size()];
          certs = certificates.toArray(certs);
        }
      } catch (CertificateException e) {
        e.printStackTrace();
      }
    }
    return certs;
  }
}
