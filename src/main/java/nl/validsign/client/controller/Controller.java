package nl.validsign.client.controller;

import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public abstract class Controller {

  /**
   * Makes the window draggable. So the user is able to drag the window.
   *
   * @param rootPane The root pane.
   */
  void makeWindowDraggable(Pane rootPane) {
    rootPane.setOnMousePressed(
        pressEvent -> {
          rootPane.setOnMouseDragged(
              dragEvent -> {
                rootPane
                    .getScene()
                    .getWindow()
                    .setX(dragEvent.getScreenX() - pressEvent.getSceneX());
                rootPane
                    .getScene()
                    .getWindow()
                    .setY(dragEvent.getScreenY() - pressEvent.getSceneY());
              });
        });
  }

  /**
   * Closes the stage.
   *
   * @param rootPane The root pane.
   */
  void closeStage(Pane rootPane) {
    Stage stage = (Stage) rootPane.getScene().getWindow();
    stage.close();
  }

  void minimizeStage(Pane rootPane) {
    Stage stage = (Stage) rootPane.getScene().getWindow();
    stage.setIconified(true);
  }
}
