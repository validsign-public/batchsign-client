package nl.validsign.client.controller;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

/** The about controller. */
public class AboutController extends Controller implements Initializable {

  /** The root pane. */
  @FXML private AnchorPane rootPane;

  /** The ok button. */
  @FXML private JFXButton okButton;

  /**
   * Closes the about window.
   *
   * @param event The action event
   */
  @FXML
  void closeAboutWindow(ActionEvent event) {
    closeStage(rootPane);
  }

  /**
   * Initialize the about controller.
   *
   * @param url The url
   * @param resourceBundle The resource bundle
   */
  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    makeWindowDraggable(rootPane);
  }
}
