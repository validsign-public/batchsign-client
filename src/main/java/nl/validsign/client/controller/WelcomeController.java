package nl.validsign.client.controller;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import nl.validsign.client.BatchSignApplication;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static nl.validsign.client.Main.WINDOW_TITLE;

public class WelcomeController implements Initializable {

  /** The logger. */
  private static final Logger logger = LogManager.getLogger();

  @FXML private AnchorPane rootPane;

  @FXML private ImageView welcomeImage;

  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    new WelcomScreen().start();
  }

  class WelcomScreen extends Thread {

    @Override
    public void run() {
      try {
        Thread.sleep(5000);

        Platform.runLater(
            new Runnable() {
              @Override
              public void run() {
                Parent root = null;
                try {
                  root = FXMLLoader.load(getClass().getResource("/fxml/start.fxml"));
                } catch (IOException e) {
                  logger.error("Could not load the splash screen.", e);
                }
                Stage primaryStage = new Stage();
                primaryStage
                    .getIcons()
                    .add(new Image(getClass().getResourceAsStream("/images/icon_validsign.png")));

                Scene mainScene = new Scene(root);
                ObservableList<String> stylesheets = mainScene.getStylesheets();
                stylesheets.addAll(
                    BatchSignApplication.class.getResource("/css/batchsign.css").toExternalForm());
                primaryStage.setTitle(WINDOW_TITLE);
                primaryStage.initStyle(StageStyle.UNDECORATED);
                primaryStage.setScene(mainScene);
                primaryStage.show();

                rootPane.getScene().getWindow().hide();
              }
            });
      } catch (InterruptedException ex) {
        logger.error("Could not load the splash screen.", ex);
      }
    }
  }
}
