package nl.validsign.client.controller;

import nl.validsign.client.ValidSignClient;
import nl.validsign.client.model.ClientProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URISyntaxException;
import java.util.Properties;

/** The properties controller. */
public class PropertiesController {

  /** The logger. */
  private static final Logger logger = LogManager.getLogger();
  /** The name of the batch sign folder. */
  private final String BATCH_SIGN_FOLDER = "BatchSign";
  /** The name of the folder for the temporarily PDF's. */
  private final String PDF_TEMP_FOLDER = "\\tempPdfs";
  /** The temporarily folder location. */
  private final String tempFolderLocation;
  /** The temporarily PDF's folder location. */
  private final String tempPdfFolderLocation;
  /** The client properties. */
  private final ClientProperties clientProperties;
  /** The configuration file. */
  private final File configFile;
  /** The public certificate file. */
  private final File publicCertificate;
  /** Use the production server. Default after start up application true. */
  private boolean useProductionServer;

  /** Constructor. */
  public PropertiesController() {
    useProductionServer = true;
    final String property = "java.io.tmpdir";
    String tempDir = System.getProperty(property);
    tempFolderLocation = tempDir + BATCH_SIGN_FOLDER;
    tempPdfFolderLocation = tempFolderLocation + PDF_TEMP_FOLDER;
    configFile = new File(tempFolderLocation + "\\config.properties");
    publicCertificate = new File(tempFolderLocation + "\\publicCertificate.crt");
    clientProperties = new ClientProperties("", "", tempPdfFolderLocation, publicCertificate);
    loadConfigFile();
  }

  /**
   * Gets the API key.
   *
   * @return API key
   */
  public String getApiKey() {
    return clientProperties.getApiKey();
  }

  /**
   * Gets the output folder.
   *
   * @return the output folder
   */
  public String getOutputFolder() {
    return clientProperties.getOutputFolder();
  }

  /**
   * Gets the public certificate.
   *
   * @return public certificate
   */
  public File getPublicCertificate() {
    return clientProperties.getPublicCertificate();
  }

  /**
   * Gets the temp PDF folder location.
   *
   * @return temp PDF folder location
   */
  public String getTempPdfFolderLocation() {
    return tempPdfFolderLocation;
  }

  /**
   * Sets use of the production server.
   *
   * @param useProductionServer Use the client the production server
   */
  public void setUseProductionServer(boolean useProductionServer) {
    this.useProductionServer = useProductionServer;
  }

  /**
   * Uses the client the production environment?
   *
   * @return uses the production server then true, otherwise false
   */
  public boolean useProductionServer() {
    return useProductionServer;
  }

  /**
   * Gets the base url of the selected environment.
   *
   * @return base url of environment
   */
  public String getBaseUrl() {
    if (useProductionServer) {
      logger.info("Request for the production environment.");
      return "https://hsm.sandbox.validsign.nl";
    }
    logger.info("Request for the sandbox environment.");
    return "https://hsm.sandbox.validsign.nl";
  }

  /**
   * Saves the properties of the client.
   *
   * @param apiKey The API key
   * @param outputFolder The output folder location
   * @throws IOException If IO error occurs
   */
  public void saveProperties(String apiKey, String outputFolder) throws IOException {
    clientProperties.setApiKey(apiKey);
    clientProperties.setOutputFolder(outputFolder);
    savePropertiesToFile(configFile);
  }

  /** Loads/creates the configuration files. */
  private void loadConfigFile() {
    try {
      if (!new File(tempFolderLocation).exists()) {
        new File(tempFolderLocation).mkdir();
      }
      if (!new File(tempPdfFolderLocation).exists()) {
        new File(tempPdfFolderLocation).mkdir();
      }
      if (!configFile.exists()) {
        savePropertiesToFile(configFile);
      }
      getClientProperties(configFile);
      loadPublicCertificateFile();
    } catch (IOException ex) {
      logger.error("Could not load configuration files.", ex);
    }
  }

  /** Loads/create public certificate file. */
  private void loadPublicCertificateFile() {
    try {
      if (!publicCertificate.exists() && !clientProperties.getApiKey().isEmpty()) {
        byte[] publicCertificateBytes =
            ValidSignClient.getPublicCertificate(clientProperties.getApiKey(), getBaseUrl());
        try (OutputStream outputStream = new FileOutputStream(publicCertificate)) {
          outputStream.write(publicCertificateBytes);
        }
      }
    } catch (IOException | URISyntaxException ex) {
      logger.error("Could not load public certificate from BatchSign server.", ex);
    }
  }

  /**
   * Saves the client properties into properties file.
   *
   * @param configFile The configuration file
   * @throws IOException If a IO error occurs
   */
  private void savePropertiesToFile(File configFile) throws IOException {
    configFile.createNewFile();
    try (FileWriter propertiesWriter = new FileWriter(configFile)) {
      Properties properties = new Properties();
      properties.setProperty("API-key", clientProperties.getApiKey());
      properties.setProperty("output-folder", clientProperties.getOutputFolder());
      properties.store(propertiesWriter, "client-settings");
    }
  }

  /**
   * Gets the client properties.
   *
   * @param configFile The configuration file
   * @throws IOException If a IO error occurs
   */
  private void getClientProperties(File configFile) throws IOException {
    try (FileReader propertiesReader = new FileReader(configFile)) {
      Properties properties = new Properties();
      properties.load(propertiesReader);
      String apiKey = properties.getProperty("API-key");
      String outputFolder = properties.getProperty("output-folder");
      clientProperties.setApiKey(apiKey);
      clientProperties.setOutputFolder(outputFolder);
    }
  }
}
