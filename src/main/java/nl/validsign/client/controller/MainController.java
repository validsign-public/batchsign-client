package nl.validsign.client.controller;

import com.jfoenix.controls.*;
import com.jfoenix.controls.events.JFXDialogEvent;
import com.mashape.unirest.http.exceptions.UnirestException;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.effect.BoxBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import nl.validsign.client.ChainLoader;
import nl.validsign.client.Main;
import nl.validsign.client.ValidSignClient;
import nl.validsign.client.model.SignDocument;
import nl.validsign.client.model.SignedHash;
import nl.validsign.client.model.Transaction;
import nl.validsign.client.model.TreeTableViewDocument;
import nl.validsign.client.service.TempFileCleanerService;
import nl.validsign.client.service.ToolbarService;
import nl.validsign.client.service.TreeTableViewService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.cert.ocsp.OCSPException;
import org.bouncycastle.operator.OperatorException;
import org.bouncycastle.x509.util.StreamParsingException;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

/** The main controller class of the Batch signing client. */
public class MainController extends Controller {

  /** The logger. */
  private static final Logger logger = LogManager.getLogger();
  /** The certificate chain error message. */
  private static final String CHAIN_ERROR_MESSAGE = "Could not load certificate chain.";
  /** The status label message. */
  private static final String STATUS_LABEL_MESSAGE = "VGhlIEFuc3dlciB0byBFdmVyeXRoaW5nIGlzIDQy";
  /** The error state. */
  private static final int ERROR_STATE = 42;
  /** The map of prepared documents. */
  private final ObservableMap<String, TreeTableViewDocument> preparedDocuments =
      FXCollections.observableHashMap();
  /** The observable list for the TreeTableView. */
  private final ObservableList<TreeTableViewDocument> entries = FXCollections.observableArrayList();
  /** The root pane. */
  @FXML private StackPane rootPane;
  /** The anchor root pane. */
  @FXML private AnchorPane rootAnchorPane;
  /** The Tree table view. */
  @FXML private JFXTreeTableView<TreeTableViewDocument> statusTreeTableView;
  /** The sign button. */
  @FXML private JFXButton signButton;
  /** The choose files button. */
  @FXML private JFXButton chooseFileButton;
  /** The toolbar menu button. */
  @FXML private JFXHamburger hamburger;
  /** The toolbar side pane. */
  @FXML private JFXDrawer drawer;
  /** The minimize window image view. */
  @FXML private ImageView minimizeWindowButton;
  /** The close window image view. */
  @FXML private ImageView closeWindowButton;
  /** The progress bar. */
  @FXML private JFXProgressBar progressBar;
  /** The status label. */
  @FXML private Label statusLabel;
  /** The list with to sign files. */
  private List<File> filesToSign = new ArrayList();
  /** The transaction information. */
  private Transaction transaction;
  /** The background thread. */
  private Thread backgroundThread;

  /**
   * Init the main window.
   *
   * @throws IOException If an IO error occurs.
   */
  @FXML
  public void initialize() throws IOException {
    // Remove all unused temp files
    TempFileCleanerService.cleanUp();

    // Add and initialize the toolbar
    ToolbarService toolbarService = new ToolbarService(hamburger, drawer);
    toolbarService.initialize();

    checkProperties();

    linkObservableData();

    // Add and initialize the tree table view
    TreeTableViewService treeTableViewService =
        new TreeTableViewService(statusTreeTableView, preparedDocuments, entries);
    treeTableViewService.initialize();

    makeWindowDraggable(rootPane);

    initWindowControlButtons();
  }

  /** Checks the properties of the application. */
  private void checkProperties() {
    String errorMessage = "";
    if (Main.propertiesController.getApiKey().isEmpty()) {
      logger.error("No API key found.");
      errorMessage += "No API found --> take a look at the settings.\n";
    }
    if (Main.propertiesController.getOutputFolder().isEmpty()) {
      logger.error("No output folder found.");
      errorMessage += "No output folder selected --> take a look at the settings.\n";
    }

    if (!errorMessage.isEmpty()) {
      errorMessage += "After changing the settings, relaunch the application.";
      showError(errorMessage);
    }
  }

  /**
   * Shows a dialog with given error message.
   *
   * @param errorMessage The error message.
   */
  private void showError(String errorMessage) {
    Platform.runLater(
        () -> {
          BoxBlur blur = new BoxBlur(3, 3, 3);
          JFXButton errorButton = new JFXButton("Ok");

          JFXDialogLayout errorDialogLayout = new JFXDialogLayout();
          Label errorLabel = new Label("Error");
          errorLabel.setTextFill(Color.web("#ff3b00"));
          errorDialogLayout.setHeading(errorLabel);
          errorDialogLayout.setBody(new Text(errorMessage));
          errorDialogLayout.setActions(errorButton);
          JFXDialog errorDialog =
              new JFXDialog(rootPane, errorDialogLayout, JFXDialog.DialogTransition.TOP);
          errorButton.addEventHandler(
              MouseEvent.MOUSE_CLICKED,
              e -> {
                errorDialog.close();
              });
          errorDialog.setOnDialogClosed(
              (JFXDialogEvent event) -> {
                // removes the blur effect from the anchor pane
                rootAnchorPane.setEffect(null);
                // Hides the progressbar
                // hide progress status
                setVisibilityOfProgress(false);
              });
          errorDialog.show();
          rootAnchorPane.setEffect(blur);
        });
  }

  /**
   * Button action to choose a file or multiple files.
   *
   * @param event The action event
   */
  @FXML
  void chooseFile(ActionEvent event) {
    // Remove all old data from map
    preparedDocuments.clear();
    entries.clear();
    statusTreeTableView.refresh();

    FileChooser fileChooser = new FileChooser();
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Hello Kitty", "*.pdf"));
    filesToSign = fileChooser.showOpenMultipleDialog(null);

    if (filesToSign == null) {
      return;
    }

    // Shows the progressbar
    setVisibilityOfProgress(true);

    Runnable prepareTask = () -> runPrepareTask();

    backgroundThread = new Thread(prepareTask);
    backgroundThread.setDaemon(true);
    backgroundThread.start();
  }

  /**
   * Button action to sign prepared documents.
   *
   * @param event The action event
   */
  @FXML
  void signDocuments(ActionEvent event) {
    setVisibilityOfSignButton(false);

    // hide progress status
    setVisibilityOfProgress(true);

    Runnable signTask = () -> runSignTask();

    backgroundThread = new Thread(signTask);
    backgroundThread.setDaemon(true);
    backgroundThread.start();
  }

  /**
   * Closes the window and stops the application.
   *
   * @param event The mouse event.
   */
  @FXML
  void closeWindow(MouseEvent event) {
    System.exit(0);
  }

  /**
   * Minimizes the window.
   *
   * @param event The mouse event.
   */
  @FXML
  void minimizeWindow(MouseEvent event) {
    minimizeStage(rootPane);
  }

  /**
   * Runs a task to prepare documents for signing.
   *
   * @throws NoSuchAlgorithmException If an algorithm error occurs.
   * @throws CertificateException If a certificate error occurs.
   * @throws NoSuchProviderException If a provider error occurs.
   * @throws InvalidKeyException If a key error occurs.
   * @throws IOException If a IO error occurs.
   */
  private void runPrepareTask() {
    try {
      ChainLoader chainLoader = new ChainLoader(Main.propertiesController.getPublicCertificate());
      String destinationFolder = Main.propertiesController.getOutputFolder();

      transaction = ValidSignClient.getTransactionId(filesToSign.size());

      if (transaction == null) {
        showError("Unable to get transaction ID from BatchSign server.");
        return;
      }

      filesToSign.forEach(
          file -> {
            try {
              SignDocument signDocument =
                  new SignDocument(
                      file,
                      destinationFolder,
                      chainLoader.getChain(),
                      Main.propertiesController.getTempPdfFolderLocation(),
                      transaction);
              signDocument.createEmptySignField();
              String documentHash = signDocument.calculateDocumentHash();
              preparedDocuments.put(documentHash, new TreeTableViewDocument(signDocument));
              statusTreeTableView.refresh();
              updateStatusLabel(filesToSign.indexOf(file), filesToSign.size());
            } catch (Exception e) {
              logger.error("Could not prepare file [{}]", file.getName(), e);
              String tempHash = UUID.randomUUID().toString();
              preparedDocuments.put(tempHash, new TreeTableViewDocument(file.getName(), tempHash));
              showError("Could not prepare document " + file.getName() + " for signing.");
            }
          });
    } catch (UnirestException e) {
      logger.error("Could not get the transaction id.", e);
      showError("Could not get the BatchSign transaction id.");
    } catch (NoSuchAlgorithmException e) {
      logger.error("Could not load certificate chain for given algorithm.", e);
      showError("Could not prepare files for given algorithm.");
    } catch (CertificateException e) {
      logger.error(CHAIN_ERROR_MESSAGE, e);
      showError(CHAIN_ERROR_MESSAGE);
    } catch (NoSuchProviderException e) {
      logger.error("The chosen provider is unsupported.", e);
      showError(CHAIN_ERROR_MESSAGE);
    } catch (InvalidKeyException e) {
      logger.error("The key is invalid for the certificate chain.", e);
      showError(CHAIN_ERROR_MESSAGE);
    } catch (IOException e) {
      logger.error("Could not load the public certificate file.", e);
      showError(CHAIN_ERROR_MESSAGE);
    }

    // hide progress status
    setVisibilityOfProgress(false);
    setVisibilityOfSignButton(true);
  }

  /** Runs a task to sign documents. */
  private void runSignTask() {
    try {
      List<SignedHash> result =
          ValidSignClient.signHashes(
              new ArrayList(preparedDocuments.keySet()), transaction.getTransactionId());
      result.forEach(
          signedHash -> {
            String key = Base64.getEncoder().encodeToString(signedHash.getOriginDocumentHash());
            SignDocument signDocument = preparedDocuments.get(key).getSignDocument();
            if (signDocument == null) {
              logger.error(
                  "Could not find a file for the given key [{}]. So the file seems to be modified.",
                  key);
              return;
            }
            signDocument.setSignedHash(signedHash.getSignedDocumentHash());
            try {
              signDocument.createSignature();
              preparedDocuments.put(key, new TreeTableViewDocument(signDocument));
              updateStatusLabel(result.indexOf(signedHash), result.size());
            } catch (IOException
                | StreamParsingException
                | GeneralSecurityException
                | OperatorException
                | OCSPException e) {
              logger.error(
                  "Could not sign the document, because something went wrong with the file [{}]",
                  signDocument.getSourceFileName(),
                  e);
              showError("Could not sign the document " + signDocument.getSourceFileName());
            }
          });
    } catch (IOException e) {
      logger.error(
          "Could not sign the document hashes, because the BatchSign server returns an error.", e);
      showError("Could not sign the document hashes.");
    } catch (URISyntaxException e) {
      logger.error("Could not do a request to the BatchSign server.", e);
      showError("Could not do a request to the BatchSign server.");
    } catch (Exception e) {
      logger.error("Could not sign document with BatchSign server.", e);
      showError("Could not sign document with the BatchSign server.");
    }

    // hide progress status
    setVisibilityOfProgress(false);
  }

  /**
   * Sets the visibility of the sign button.
   *
   * @param visible Is the button visible and clickable
   */
  void setVisibilityOfSignButton(boolean visible) {
    if (visible) {
      signButton.setDisable(false);
      return;
    }
    signButton.setDisable(true);
  }

  /**
   * Updates the status label.
   *
   * @param currentNumber The current number of loaded elements.
   * @param completedNumber The total number of loaded elements.
   */
  void updateStatusLabel(int currentNumber, int completedNumber) {
    Platform.runLater(
        () -> {
          if (currentNumber != ERROR_STATE) {
            String text =
                new StringBuilder()
                    .append(currentNumber)
                    .append(" of ")
                    .append(completedNumber)
                    .toString();
            statusLabel.setText(text);
          } else {
            statusLabel.setText(new String(Base64.getDecoder().decode(STATUS_LABEL_MESSAGE)));
          }
        });
  }

  /**
   * Sets the visibility of the progress status. The visibility will changed for the progressbar and
   * the status text.
   *
   * @param visible Is the progress status visible or not
   */
  void setVisibilityOfProgress(boolean visible) {
    progressBar.setVisible(visible);
    statusLabel.setVisible(visible);
  }

  /**
   * Link the observable mpa data to a observable list. This step is necessary, because a
   * TreeTableView works with a list and not with a map.
   */
  void linkObservableData() {
    preparedDocuments.addListener(
        (MapChangeListener.Change<? extends String, ? extends TreeTableViewDocument> change) -> {
          boolean removed = change.wasRemoved();
          if (removed != change.wasAdded()) {
            if (removed) {
              // no put for existing key
              // remove pair completely
              entries.remove(change.getValueRemoved());
            } else {
              // add new entry
              entries.add(change.getValueAdded());
            }
          } else {
            // update the data table values
            entries.removeIf(document -> document.getDocumentHash().equals(change.getKey()));
            entries.add(change.getValueAdded());
          }
          statusTreeTableView.sort();
        });
  }

  /** Init the window controll buttons, to minimize and close the window. */
  void initWindowControlButtons() {
    // minimize window imageview with hover effect and click handler
    minimizeWindowButton
        .hoverProperty()
        .addListener(
            (obs, oldValue, newValue) -> {
              if (newValue) {
                minimizeWindowButton.setImage(
                    new Image(getClass().getResourceAsStream("/images/minimize_filled.png")));
              } else {
                minimizeWindowButton.setImage(
                    new Image(getClass().getResourceAsStream("/images/minimize.png")));
              }
            });

    // close window imageview with hover effect and click handler
    closeWindowButton
        .hoverProperty()
        .addListener(
            (obs, oldValue, newValue) -> {
              if (newValue) {
                closeWindowButton.setImage(
                    new Image(getClass().getResourceAsStream("/images/close_filled.png")));
              } else {
                closeWindowButton.setImage(
                    new Image(getClass().getResourceAsStream("/images/close.png")));
              }
            });
  }
}
