package nl.validsign.client.controller;

import com.jfoenix.controls.JFXButton;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import nl.validsign.client.BatchSignApplication;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

import static nl.validsign.client.Main.WINDOW_TITLE;

/** The toolbar controller. */
public class ToolbarController {

  /** The logger. */
  private static final Logger logger = LogManager.getLogger();

  /** The file path of the preferences dialog. */
  private static final String PREFERENCE_DIALOG_PATH = "/fxml/preferenceDialog.fxml";

  /** The file path of the about dialog. */
  private static final String ABOUT_DIALOG_PATH = "/fxml/about.fxml";

  /** The settings button. */
  @FXML private JFXButton settingsButton;

  /** The about button. */
  @FXML private JFXButton aboutButton;

  /** The quit button. */
  @FXML private JFXButton quitButton;

  /**
   * Quits the application.
   *
   * @param event The action event
   */
  @FXML
  void quit(ActionEvent event) {
    closeWindow();
  }

  /**
   * Shows the about screen.
   *
   * @param event The action event
   * @throws IOException If an IO error occurs
   */
  @FXML
  void showAbout(ActionEvent event) {
    try {
      showDialog(ABOUT_DIALOG_PATH);
    } catch (IOException e) {
      logger.error("Could not load about dialog fxml file.", e);
    }
  }

  /**
   * Shows the preferences screen.
   *
   * @param event The action event
   */
  @FXML
  void showSettings(ActionEvent event) {
    try {
      showDialog(PREFERENCE_DIALOG_PATH);
    } catch (IOException e) {
      logger.error("Could not load preferences dialog fxml file.", e);
    }
  }

  /**
   * Shows the preference dialog.
   *
   * @throws IOException If IO error occurs.
   */
  private void showDialog(String fxmlPath) throws IOException {
    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(fxmlPath));
    Parent parent = fxmlLoader.load();
    Scene scene = new Scene(parent);
    ObservableList<String> stylesheets = scene.getStylesheets();
    stylesheets.addAll(
        BatchSignApplication.class.getResource("/css/batchsign.css").toExternalForm());
    Stage stage = new Stage();
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.initStyle(StageStyle.UNDECORATED);
    stage.setTitle(WINDOW_TITLE);
    stage.getIcons().add(new Image(getClass().getResourceAsStream("/images/icon_validsign.png")));
    stage.setScene(scene);
    stage.showAndWait();
  }

  /** Close the application window. */
  private void closeWindow() {
    System.exit(0);
  }
}
