package nl.validsign.client.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import nl.validsign.client.Main;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/** The preference dialog controller. */
public class PreferenceDialogController extends Controller implements Initializable {

  /** The root pane. */
  @FXML private VBox rootPane;

  /** The anchor root pane. */
  @FXML private AnchorPane rootAnchorPane;

  /** The save button. */
  @FXML private JFXButton saveButton;

  /** The cancel button. */
  @FXML private JFXButton cancelButton;

  /** The API key text input. */
  @FXML private JFXTextField apiKeyTextInput;

  /** The output folder label. */
  @FXML private JFXTextField outputFolderTextInput;

  /** The edit output folder button. */
  @FXML private JFXButton editOutputFolderButton;

  /** The environment toggle button. */
  @FXML private ToggleButton environmentToggle;

  /**
   * Init the preferences dialog.
   *
   * @param url The url.
   * @param resourceBundle The resource bundle.
   */
  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    apiKeyTextInput.setText(Main.propertiesController.getApiKey());
    outputFolderTextInput.setText(Main.propertiesController.getOutputFolder());

    makeWindowDraggable(rootPane);
  }

  /**
   * Changes the environment to sends the request to.
   *
   * @param event The action event.
   */
  @FXML
  void changeEnvironment(ActionEvent event) {
    if (Main.propertiesController.useProductionServer()) {
      environmentToggle.setText("Sandbox");
      Main.propertiesController.setUseProductionServer(false);
    } else {
      environmentToggle.setText("Production");
      Main.propertiesController.setUseProductionServer(true);
    }
  }

  /**
   * Shows the current environment.
   *
   * @param event The mouse event.
   */
  @FXML
  void showEnvironmentToggle(MouseEvent event) {
    if (event.getClickCount() >= 5) {
      environmentToggle.setVisible(true);
      if (Main.propertiesController.useProductionServer()) {
        environmentToggle.setText("Production");
        environmentToggle.setSelected(false);
      } else {
        environmentToggle.setText("Sandbox");
        environmentToggle.setSelected(true);
      }
    }
  }

  /**
   * The button action for cancel.
   *
   * @param event The action event
   */
  @FXML
  void cancel(ActionEvent event) {
    closeStage(rootPane);
  }

  /**
   * The button action to edit the output folder.
   *
   * @param event The action event
   */
  @FXML
  void editOutputFolder(ActionEvent event) {
    DirectoryChooser directoryChooser = new DirectoryChooser();
    File selectedFolder = directoryChooser.showDialog(null);

    if (selectedFolder != null) {
      if (selectedFolder.getAbsolutePath() != null) {
        outputFolderTextInput.setText(selectedFolder.getAbsolutePath());
      }
    }
  }

  /**
   * The button action to save all preferences.
   *
   * @param event The action event
   * @throws IOException If a IO error occurs
   */
  @FXML
  void savePreferences(ActionEvent event) throws IOException {
    String apiKey = apiKeyTextInput.getText();
    String outputFolder = outputFolderTextInput.getText();

    Main.propertiesController.saveProperties(apiKey, outputFolder);
    closeStage(rootPane);
  }
}
