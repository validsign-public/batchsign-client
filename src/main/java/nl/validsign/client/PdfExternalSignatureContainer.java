package nl.validsign.client;

import com.itextpdf.kernel.pdf.PdfDictionary;
import com.itextpdf.signatures.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

/** External PDF signature container. */
public class PdfExternalSignatureContainer implements IExternalSignatureContainer {

  /** The logger. */
  private static final Logger logger = LogManager.getLogger();

  /** The URL for the Time Stamp Authority (TSA) */
  private static final String TSA_URL = "http://timestamp.entrust.net/TSS/RFC3161sha2TS";

  /** The hash algorithm */
  private static final String HASH_ALGORITHM = "SHA-256";

  /** The signature instance */
  private static final String SIGNATURE_INSTANCE = "SHA256withRSA";

  /** The certificate chain */
  private final Certificate[] chain;

  /** The signed hash */
  private final byte[] signedHash;

  /**
   * Constructor.
   *
   * @param chain The certificate chain
   * @param signedHash The signed hash
   */
  public PdfExternalSignatureContainer(Certificate[] chain, byte[] signedHash) {
    this.chain = chain;
    this.signedHash = signedHash;
  }

  /**
   * Signs and verifies a document signature.
   *
   * @param inputStream The input stream of the to sign document
   * @return the signature bytes
   * @throws GeneralSecurityException If the signature is not correct
   */
  @Override
  public byte[] sign(InputStream inputStream) throws GeneralSecurityException {
    try {
      BouncyCastleDigest digest = new BouncyCastleDigest();
      PdfPKCS7 signer = new PdfPKCS7(null, chain, HASH_ALGORITHM, null, digest, false);

      String url = CertificateUtil.getOCSPURL((X509Certificate) chain[0]);
      byte[] ocsp =
          new OcspClientBouncyCastle(new OCSPVerifier(null, null))
              .getEncoded((X509Certificate) chain[0], (X509Certificate) chain[1], url);

      byte[] documentHash =
          DigestAlgorithms.digest(inputStream, digest.getMessageDigest(HASH_ALGORITHM));

      byte[] authenticatedAttributeBytes =
          signer.getAuthenticatedAttributeBytes(
              documentHash, ocsp, null, PdfSigner.CryptoStandard.CMS);

      signer.setExternalDigest(signedHash, null, "RSA");

      Signature signature = Signature.getInstance(SIGNATURE_INSTANCE);
      signature.initVerify(chain[0].getPublicKey());
      signature.update(authenticatedAttributeBytes);
      if (!signature.verify(signedHash)) {
        throw new GeneralSecurityException("Signature is not correct.");
      }

      ITSAClient tsaClient = new TSAClientBouncyCastle(TSA_URL, null, null);

      return signer.getEncodedPKCS7(
          documentHash, tsaClient, ocsp, null, PdfSigner.CryptoStandard.CMS);
    } catch (Exception e) {
      logger.error("Could not sign document.", e);
    }
    return null;
  }

  @Override
  public void modifySigningDictionary(PdfDictionary pdfDictionary) {}
}
