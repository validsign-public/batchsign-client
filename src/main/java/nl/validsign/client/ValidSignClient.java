package nl.validsign.client;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import nl.validsign.client.model.SignedHash;
import nl.validsign.client.model.Transaction;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/** The ValidSign client class. */
public class ValidSignClient {

  /** The logger. */
  private static final Logger logger = LogManager.getLogger();

  /** The amount parameter describes the number of to sign documents */
  private static final String AMOUNT_PARAM = "amount";

  /** The URL for sign single hash */
  private static final String SIGN_SINGLE_URL = "/sign/document";

  /** The URL for sign multiple hashes */
  private static final String SIGN_MULTIPLE_URL = "/sign/documents";

  /** The URL to get the public certificate */
  private static final String GET_CERTIFICATE_URL = "/sign/get/certificate";

  /** The URL to get the transaction id */
  private static final String GET_TRANSACTION_ID_URL = "/sign/get/transactionid";

  /**
   * Signs a number of hashes.
   *
   * @param documentHashes The document hashes
   * @return list of signed hashes
   * @throws IOException If a IO error occurs
   * @throws URISyntaxException If a URI error occurs
   */
  public static List<SignedHash> signHashes(List<String> documentHashes, String transactionId)
      throws IOException, URISyntaxException {
    List<String> listWithoutErrors = removeErrorHashes(documentHashes);
    if (listWithoutErrors.size() == 1) {
      return signSingleHash(listWithoutErrors.get(0), transactionId);
    } else {
      return signMultipleHashes(listWithoutErrors, transactionId);
    }
  }

  /**
   * Removes all documents with prepare errors.
   *
   * @param documentHashes List of all document hashes
   * @return filter list without error document hashes
   */
  private static List<String> removeErrorHashes(List<String> documentHashes) {
    return documentHashes.stream().filter(item -> item.length() > 40).collect(Collectors.toList());
  }

  /**
   * Gets the transaction id.
   *
   * @param amount The amount of to sign hashes
   * @return transaction
   * @throws IOException If a IO error occurs
   * @throws URISyntaxException If a URI error occurs
   * @throws UnirestException If a Uni rest error occurs
   */
  public static Transaction getTransactionId(int amount) throws UnirestException {
    Unirest.setTimeouts(0, 0);
    HttpResponse<String> response =
        Unirest.get(
                Main.propertiesController.getBaseUrl()
                    + GET_TRANSACTION_ID_URL
                    + "?"
                    + AMOUNT_PARAM
                    + "="
                    + amount)
            .header("Authorization", Main.propertiesController.getApiKey())
            .asString();
    if (response.getStatus() == HttpStatus.SC_OK) {
      JSONObject jsonResponse = new JSONObject(response.getBody());
      String reason = jsonResponse.getString("reason");
      String location = jsonResponse.getString("location");
      String transactionId = jsonResponse.getString("transactionId");
      return new Transaction(reason, location, transactionId);
    }
    logger.error(
        "Could not get transaction id from BatchSign server with response [{}], [{}], [{}]",
        response.getStatus(),
        response.getStatusText(),
        response.getBody());
    return null;
  }

  /**
   * Gets the public certificate.
   *
   * @param apiKey The API key
   * @return public certificate
   * @throws IOException If a IO error occurs
   * @throws URISyntaxException If a URI error occurs
   */
  public static byte[] getPublicCertificate(String apiKey, String baseUrl)
      throws IOException, URISyntaxException {
    URL url = new URL(baseUrl + GET_CERTIFICATE_URL);
    HttpGet get = new HttpGet(url.toURI());

    get.setHeader(new BasicHeader("Authorization", apiKey));
    get.setHeader(new BasicHeader("Content-Type", "application/json"));
    try (CloseableHttpClient httpClient = HttpClients.createDefault();
         CloseableHttpResponse response = httpClient.execute(get)) {

      if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        return response.getEntity().getContent().readAllBytes();
      }
      logger.error(
          "Could not get the public certificate from BatchSign server with response [{}], [{}], [{}]",
          response.getStatusLine().getStatusCode(),
          response.getStatusLine().getReasonPhrase(),
          response.getEntity().getContent());
    }
    return new byte[0];
  }

  /**
   * Gets the public certificate.
   *
   * @return public certificate
   * @throws IOException If a IO error occurs
   * @throws URISyntaxException If a URI error occurs
   */
  public static byte[] getPublicCertificate() throws IOException, URISyntaxException {
    URL url = new URL(Main.propertiesController.getBaseUrl() + GET_CERTIFICATE_URL);
    HttpGet get = new HttpGet(url.toURI());

    get.setHeader(new BasicHeader("Authorization", Main.propertiesController.getApiKey()));
    get.setHeader(new BasicHeader("Content-Type", "application/json"));
    try (CloseableHttpClient httpClient = HttpClients.createDefault();
         CloseableHttpResponse response = httpClient.execute(get)) {

      if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        return response.getEntity().getContent().readAllBytes();
      }
      logger.error(
          "Could not get the public certificate from BatchSign server with response [{}], [{}], [{}]",
          response.getStatusLine().getStatusCode(),
          response.getStatusLine().getReasonPhrase(),
          response.getEntity().getContent());
    }
    return new byte[0];
  }

  /**
   * Signs a single hash.
   *
   * @param documentHash The document hash
   * @return list of signed hash, within one signed hash
   * @throws IOException If a IO error occurs
   * @throws URISyntaxException If a URI error occurs
   */
  private static List<SignedHash> signSingleHash(String documentHash, String transactionId)
      throws IOException, URISyntaxException {
    URL url = new URL(Main.propertiesController.getBaseUrl() + SIGN_SINGLE_URL);
    HttpPost post = new HttpPost(url.toURI());

    List<NameValuePair> urlParameters = new ArrayList();

    urlParameters.add(new BasicNameValuePair("document_hash", documentHash));
    urlParameters.add(new BasicNameValuePair("transaction_id", transactionId));
    post.setEntity(new UrlEncodedFormEntity(urlParameters));
    post.setHeader(new BasicHeader("Authorization", Main.propertiesController.getApiKey()));

    try (CloseableHttpClient httpClient = HttpClients.createDefault();
         CloseableHttpResponse response = httpClient.execute(post)) {

      if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        String responseString = EntityUtils.toString(response.getEntity());
        JSONObject jsonResponse = new JSONObject(responseString);

        JSONArray jsonArray = new JSONArray(jsonResponse.get("signedDocuments").toString());
        JSONObject jsonSignedDocument = jsonArray.getJSONObject(0);
        byte[] signedHash =
            Base64.getDecoder().decode(jsonSignedDocument.getString("signedDocumentHash"));
        byte[] originHash =
            Base64.getDecoder().decode(jsonSignedDocument.getString("originDocumentHash"));
        ArrayList signedHashes = new ArrayList();
        signedHashes.add(new SignedHash(originHash, signedHash));

        return signedHashes;
      }

      logger.error(
          "Could not sign document hash with BatchSign server with response [{}], [{}], [{}]",
          response.getStatusLine().getStatusCode(),
          response.getStatusLine().getReasonPhrase(),
          response.getEntity().getContent());

      return Collections.EMPTY_LIST;
    }
  }

  /**
   * Signs multiple hashes.
   *
   * @param documentHashes The document hashes
   * @return list of signed hash
   * @throws IOException If a IO error occurs
   * @throws URISyntaxException If a URI error occurs
   */
  private static List<SignedHash> signMultipleHashes(
          List<String> documentHashes, String transactionId) throws IOException, URISyntaxException {
    URL url = new URL(Main.propertiesController.getBaseUrl() + SIGN_MULTIPLE_URL);
    HttpPost post = new HttpPost(url.toURI());

    List<NameValuePair> urlParameters = new ArrayList();

    JSONArray jsonSendData = new JSONArray();
    documentHashes.forEach(jsonSendData::put);

    urlParameters.add(new BasicNameValuePair("document_hashes", jsonSendData.toString()));
    urlParameters.add(new BasicNameValuePair("transaction_id", transactionId));
    post.setEntity(new UrlEncodedFormEntity(urlParameters));
    post.setHeader(new BasicHeader("Authorization", Main.propertiesController.getApiKey()));

    try (CloseableHttpClient httpClient = HttpClients.createDefault();
         CloseableHttpResponse response = httpClient.execute(post)) {

      if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        String responseString = EntityUtils.toString(response.getEntity());

        JSONObject jsonResponse = new JSONObject(responseString);

        List<SignedHash> signedHashes = new ArrayList();
        JSONArray jsonArray = new JSONArray(jsonResponse.get("signedDocuments").toString());
        for (int i = 0; i < jsonArray.length(); i++) {
          JSONObject jsonSignedDocument = jsonArray.getJSONObject(i);
          byte[] signedHash =
              Base64.getDecoder().decode(jsonSignedDocument.getString("signedDocumentHash"));
          byte[] originHash =
              Base64.getDecoder().decode(jsonSignedDocument.getString("originDocumentHash"));
          signedHashes.add(new SignedHash(originHash, signedHash));
        }

        return signedHashes;
      }

      logger.error(
          "Could not sign document hashes with BatchSign server with response [{}], [{}], [{}]",
          response.getStatusLine().getStatusCode(),
          response.getStatusLine().getReasonPhrase(),
          response.getEntity().getContent());

      return Collections.EMPTY_LIST;
    }
  }
}
