package nl.validsign.client.service;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBasicCloseTransition;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXMLLoader;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

import java.io.IOException;

/** The toolbar service, to handle all toolbar actions. */
public class ToolbarService {

  /** The toolbar menu button. */
  private final JFXHamburger hamburger;
  /** The toolbar side pane. */
  private final JFXDrawer drawer;

  /**
   * Constructor.
   *
   * @param hamburger The hamburger menu.
   * @param drawer The side pane.
   */
  public ToolbarService(JFXHamburger hamburger, JFXDrawer drawer) {
    this.hamburger = hamburger;
    this.drawer = drawer;
  }

  /**
   * Initializes the toolbar parts.
   *
   * @throws IOException If an IO error occurs
   */
  public void initialize() throws IOException {
    initToolbar();
    initToolbarButton();
  }

  /**
   * Init the toolbar.
   *
   * @throws IOException If an IO error occurs
   */
  private void initToolbar() throws IOException {
    AnchorPane toolbar = FXMLLoader.load(getClass().getResource("/fxml/toolbar.fxml"));
    drawer.setSidePane(toolbar);
    drawer.setOverLayVisible(false);
  }

  /** Init the toolbar menu button. */
  private void initToolbarButton() {
    HamburgerBasicCloseTransition burgerTask = new HamburgerBasicCloseTransition(hamburger);
    burgerTask.setRate(-1);
    hamburger.addEventHandler(
        MouseEvent.MOUSE_PRESSED,
        (e) -> {
          burgerTask.setRate(burgerTask.getRate() * -1);
          burgerTask.play();

          if (drawer.isOpened()) {
            // Close the toolbar
            drawer.close();

            // Move the toolbar button back to start position
            startToolbarAnimation(20);
          } else {
            // Move the toolbar button to new expand position
            startToolbarAnimation(220);

            // Open toolbar
            drawer.open();
          }
        });
  }

  /**
   * Starts the toolbar animation to a given end position.
   *
   * @param endPosition The end position of the toolbar
   */
  private void startToolbarAnimation(int endPosition) {
    Timeline timeline = new Timeline();
    timeline.setAutoReverse(false);
    KeyValue keyValue =
        new KeyValue(hamburger.layoutXProperty(), endPosition, Interpolator.EASE_IN);
    KeyFrame keyFrame = new KeyFrame(Duration.millis(400), keyValue);
    timeline.getKeyFrames().add(keyFrame);
    timeline.play();
  }
}
