package nl.validsign.client.service;

import nl.validsign.client.Main;

import java.io.File;
import java.util.Arrays;

/** Remove all unused temp files. */
public class TempFileCleanerService {

  public static void cleanUp() {
    File tempDir = new File(Main.propertiesController.getTempPdfFolderLocation());
    Arrays.stream(tempDir.listFiles()).forEach(File::delete);
  }
}
