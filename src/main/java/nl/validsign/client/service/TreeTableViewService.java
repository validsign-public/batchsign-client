package nl.validsign.client.service;

import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import nl.validsign.client.model.SignDocument;
import nl.validsign.client.model.TreeTableViewDocument;

import java.util.Comparator;

public class TreeTableViewService {

  /** TreeTableView icon size */
  private static final double ICON_SIZE = 24;
  /** The text column width */
  private static final double TEXT_COLUMN_WIDTH = 250;
  /** The icon column width */
  private static final double ICON_COLUMN_WIDTH = 165;
  /** The Tree table view. */
  private final JFXTreeTableView<TreeTableViewDocument> statusTreeTableView;
  /** The observable list for the TreeTableView. */
  private final ObservableList<TreeTableViewDocument> entries;
  /** The map of prepared documents. */
  private final ObservableMap<String, TreeTableViewDocument> preparedDocuments;
  /** The global visible button. */
  private final ImageView globalVisibleButton;
  /** The global visible status. */
  private boolean globalVisible;

  /**
   * Constructor.
   *
   * @param statusTreeTableView The JFX TreeTableView
   * @param preparedDocuments The prepared documents
   * @param entries The entries list
   */
  public TreeTableViewService(
          JFXTreeTableView<TreeTableViewDocument> statusTreeTableView,
          ObservableMap<String, TreeTableViewDocument> preparedDocuments,
          ObservableList<TreeTableViewDocument> entries) {
    this.statusTreeTableView = statusTreeTableView;
    this.preparedDocuments = preparedDocuments;
    this.entries = entries;
    globalVisibleButton =
        new ImageView(new Image(getClass().getResourceAsStream("/images/invisible_signature.png")));
    globalVisible = false;
  }

  /** Initializes the TreeTableView. */
  public void initialize() {
    initTreeTableView();
  }

  /**
   * Resize the image, to fit into the column.
   *
   * @param image The image
   */
  private void resizeImage(ImageView image) {
    image.setFitHeight(ICON_SIZE);
    image.setFitWidth(ICON_SIZE);
    image.setPreserveRatio(true);
  }

  /**
   * Sets the given column fixed, so it is no more editable, sortable, resizeable and recordable.
   *
   * @param column The column
   */
  private void setColumnFixed(TreeTableColumn column) {
    column.setEditable(false);
    column.setSortable(false);
    column.setResizable(false);
    column.setReorderable(false);
  }

  /**
   * Inits the document column.
   *
   * @return document column
   */
  private TreeTableColumn<TreeTableViewDocument, String> initDocumentColumn() {
    // Init document column
    TreeTableColumn<TreeTableViewDocument, String> documentColumn =
        new TreeTableColumn<>("Document");
    documentColumn.setCellValueFactory(new TreeItemPropertyValueFactory<>("name"));
    documentColumn.setPrefWidth(TEXT_COLUMN_WIDTH);
    documentColumn.setStyle("-fx-alignment: CENTER-LEFT;");
    setColumnFixed(documentColumn);

    return documentColumn;
  }

  /**
   * Inits the prepared column
   *
   * @return prepared column
   */
  private TreeTableColumn<TreeTableViewDocument, Boolean> initPreparedColumn() {
    // Init prepared column
    TreeTableColumn<TreeTableViewDocument, Boolean> preparedColumn =
        new TreeTableColumn<>("Prepared");
    preparedColumn.setCellValueFactory(
        item -> new SimpleBooleanProperty(item.getValue().getValue().isPrepared()));
    preparedColumn.setPrefWidth(ICON_COLUMN_WIDTH);
    setColumnFixed(preparedColumn);

    // cell factory to display graphic
    preparedColumn.setCellFactory(
        ttc ->
            new TreeTableCell<>() {

              private final ImageView imagePreparedFailed =
                  new ImageView(
                      new Image(getClass().getResourceAsStream("/images/preparing_failed.png")));
              private final ImageView imagePreparedSuccess =
                  new ImageView(new Image(getClass().getResourceAsStream("/images/prepared.png")));

              /** Updates the image of the prepared cell. */
              @Override
              protected void updateItem(Boolean item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null) {
                  setText(null);
                  if (item) {
                    resizeImage(imagePreparedSuccess);
                    setGraphic(imagePreparedSuccess);
                  } else {
                    resizeImage(imagePreparedFailed);
                    setGraphic(imagePreparedFailed);
                  }
                }
              }
            });

    return preparedColumn;
  }

  /**
   * Inits the signed column
   *
   * @return signed column
   */
  private TreeTableColumn<TreeTableViewDocument, Boolean> initSignedColumn() {
    // Init signed column
    TreeTableColumn<TreeTableViewDocument, Boolean> signedColumn = new TreeTableColumn<>("Signed");
    signedColumn.setCellValueFactory(
        item -> new SimpleBooleanProperty(item.getValue().getValue().isSignatureValid()));
    signedColumn.setPrefWidth(ICON_COLUMN_WIDTH);
    setColumnFixed(signedColumn);

    // cell factory to display graphic
    signedColumn.setCellFactory(
        ttc ->
            new TreeTableCell<>() {

              private final ImageView imageSignatureFailed =
                  new ImageView(
                      new Image(getClass().getResourceAsStream("/images/signature_failed.png")));
              private final ImageView imageSignatureSuccess =
                  new ImageView(
                      new Image(getClass().getResourceAsStream("/images/signature_correct.png")));

              /** Updates the image of the signed cell. */
              @Override
              protected void updateItem(Boolean item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null) {
                  setText(null);
                  if (item) {
                    resizeImage(imageSignatureSuccess);
                    setGraphic(imageSignatureSuccess);
                  } else {
                    resizeImage(imageSignatureFailed);
                    setGraphic(imageSignatureFailed);
                  }
                }
              }
            });

    return signedColumn;
  }

  /**
   * Inits the visible signature column
   *
   * @return visible signature column
   */
  private TreeTableColumn<TreeTableViewDocument, Boolean> initVisibleSignatureColumn() {
    // Init visible signature column
    TreeTableColumn<TreeTableViewDocument, Boolean> visibleSignatureColumn =
        new TreeTableColumn<>("Visible Signature");
    visibleSignatureColumn.setCellValueFactory(
        item -> new SimpleBooleanProperty(item.getValue().getValue().isSignatureVisible()));
    visibleSignatureColumn.setPrefWidth(ICON_COLUMN_WIDTH);
    setColumnFixed(visibleSignatureColumn);

    resizeImage(globalVisibleButton);
    visibleSignatureColumn.setGraphic(globalVisibleButton);

    // cell factory to display graphic
    visibleSignatureColumn.setCellFactory(
        ttc ->
            new TreeTableCell<>() {

              private final ImageView imageInvisibleSignature =
                  new ImageView(
                      new Image(getClass().getResourceAsStream("/images/invisible_signature.png")));
              private final ImageView imageVisibleSignature =
                  new ImageView(
                      new Image(getClass().getResourceAsStream("/images/visible_signature.png")));

              /** Updates the image of the visible signature cell. */
              @Override
              protected void updateItem(Boolean item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null) {
                  setText(null);
                  if (item) {
                    resizeImage(imageVisibleSignature);
                    setGraphic(imageVisibleSignature);
                  } else {
                    resizeImage(imageInvisibleSignature);
                    setGraphic(imageInvisibleSignature);
                  }
                }
              }
            });

    return visibleSignatureColumn;
  }

  /** Init tree table view */
  private void initTreeTableView() {
    TreeTableColumn documentColumn = initDocumentColumn();
    TreeTableColumn preparedColumn = initPreparedColumn();
    TreeTableColumn signedColumn = initSignedColumn();
    TreeTableColumn visibleSignatureColumn = initVisibleSignatureColumn();

    // Init root element
    TreeItem<TreeTableViewDocument> root =
        new RecursiveTreeItem<>(entries, RecursiveTreeObject::getChildren);
    // Add all columns to the table
    statusTreeTableView
        .getColumns()
        .addAll(documentColumn, preparedColumn, signedColumn, visibleSignatureColumn);

    // add click handler to change signature visibility
    addHeaderClickListener();
    // Link the root with all data to the table
    statusTreeTableView.setRoot(root);
    // Set the sort policy to sort columns by file name
    addSortPolicy();
  }

  /** Add click listener for header. */
  void addHeaderClickListener() {
    statusTreeTableView.addEventHandler(
        MouseEvent.MOUSE_CLICKED,
        e -> {
          // checks if user clicks in the content container
          if (e.getSceneY() >= 108) {
            if (statusTreeTableView.getSelectionModel().getSelectedItem() != null) {
              TreeTableViewDocument treeTableViewDocument =
                  statusTreeTableView.getSelectionModel().getSelectedItem().getValue();
              SignDocument document = treeTableViewDocument.getSignDocument();
              if (document != null) {
                boolean isVisible = !document.isVisibleSignature();
                document.setVisibleSignature(isVisible);
                preparedDocuments.put(
                    treeTableViewDocument.getDocumentHash(), new TreeTableViewDocument(document));
              }
            }
          } else {
            // checks if user clicks in visible signature header
            if (e.getSceneX() >= 603 && e.getSceneX() <= 765) {
              globalVisible = !globalVisible;
              if (globalVisible) {
                globalVisibleButton.setImage(
                    new Image(getClass().getResourceAsStream("/images/visible_signature.png")));
              } else {
                globalVisibleButton.setImage(
                    new Image(getClass().getResourceAsStream("/images/invisible_signature.png")));
              }
              updateVisibilityOfAllDocuments();
            }
          }
        });
  }

  /** Set the sort policy to sort columns by file name */
  void addSortPolicy() {
    statusTreeTableView.setSortPolicy(
        t -> {
          Comparator<TreeItem<TreeTableViewDocument>> itemComparator =
              (item1, item2) -> {
                try {
                  int item1Value = Integer.parseInt(item1.getValue().getName());
                  int item2Value = Integer.parseInt(item2.getValue().getName());
                  return Integer.compare(item2Value, item1Value);
                } catch (NumberFormatException e) {
                  return item2.getValue().getName().compareToIgnoreCase(item1.getValue().getName());
                }
              };
          FXCollections.sort(statusTreeTableView.getRoot().getChildren(), itemComparator);

          return true;
        });
  }

  /** Updates the signature visibility for each document. */
  void updateVisibilityOfAllDocuments() {
    for (TreeTableViewDocument document : preparedDocuments.values()) {
      // checks if the document is correct prepared
      if (document.getSignDocument() != null) {
        SignDocument signDocument = document.getSignDocument();
        signDocument.setVisibleSignature(globalVisible);
        preparedDocuments.put(document.getDocumentHash(), new TreeTableViewDocument(signDocument));
      }
    }
  }
}
