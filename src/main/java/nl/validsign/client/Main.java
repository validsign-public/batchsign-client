package nl.validsign.client;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import nl.validsign.client.controller.PropertiesController;

/** The main class of the BatchSign application */
public class Main extends Application {

  public static final String WINDOW_TITLE = "BatchSign";

  /** The properties controller */
  public static final PropertiesController propertiesController = new PropertiesController();

  /**
   * Main method to start the application.
   *
   * @param args The arguments
   */
  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    Parent root = FXMLLoader.load(getClass().getResource("/fxml/welcome.fxml"));
    Scene scene = new Scene(root);
    ObservableList<String> stylesheets = scene.getStylesheets();
    stylesheets.addAll(
        BatchSignApplication.class.getResource("/css/batchsign.css").toExternalForm());
    primaryStage.initStyle(StageStyle.UNDECORATED);
    primaryStage
        .getIcons()
        .add(new Image(getClass().getResourceAsStream("/images/icon_validsign.png")));
    primaryStage.setTitle(WINDOW_TITLE);
    primaryStage.setScene(scene);
    primaryStage.show();
  }
}
