package nl.validsign.client.model;

/** The transaction class. */
public class Transaction {

  /** The transaction reason code. */
  private final String reason;

  /** The transaction location code. */
  private final String location;

  /** The transaction id. */
  private final String transactionId;

  /**
   * Constructor.
   *
   * @param reason The reason code
   * @param location The location code
   * @param transactionId The transaction id
   */
  public Transaction(String reason, String location, String transactionId) {
    this.reason = reason;
    this.location = location;
    this.transactionId = transactionId;
  }

  /**
   * Gets the reason code.
   *
   * @return reason
   */
  public String getReason() {
    return reason;
  }

  /**
   * Gets the location code.
   *
   * @return location
   */
  public String getLocation() {
    return location;
  }

  /**
   * Gets the transaction id.
   *
   * @return transaction id
   */
  public String getTransactionId() {
    return transactionId;
  }
}
