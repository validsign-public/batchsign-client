package nl.validsign.client.model;

/** The signed hash object. */
public class SignedHash {

  /** The origin document hash */
  private final byte[] originDocumentHash;

  /** The signed document hash */
  private final byte[] signedDocumentHash;

  /**
   * Constructor.
   *
   * @param signedDocumentHash The signed document hash
   */
  public SignedHash(byte[] originDocumentHash, byte[] signedDocumentHash) {
    this.originDocumentHash = originDocumentHash;
    this.signedDocumentHash = signedDocumentHash;
  }

  /**
   * Gets the origin document hash.
   *
   * @return origin document hash.
   */
  public byte[] getOriginDocumentHash() {
    return originDocumentHash;
  }

  /**
   * Gets the signed document hash.
   *
   * @return signed document hash
   */
  public byte[] getSignedDocumentHash() {
    return signedDocumentHash;
  }
}
