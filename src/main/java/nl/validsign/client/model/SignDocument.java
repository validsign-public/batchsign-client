package nl.validsign.client.model;

import com.itextpdf.io.source.IRandomAccessSource;
import com.itextpdf.io.source.RASInputStream;
import com.itextpdf.io.source.RandomAccessSourceFactory;
import com.itextpdf.kernel.pdf.*;
import com.itextpdf.signatures.*;
import nl.validsign.client.PdfExternalSignatureContainer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.cert.ocsp.OCSPException;
import org.bouncycastle.operator.OperatorException;
import org.bouncycastle.x509.util.StreamParsingException;

import java.io.*;
import java.security.GeneralSecurityException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Base64;

/** Sign document object */
public class SignDocument {

  /** The logger. */
  private static final Logger logger = LogManager.getLogger();

  /** The field name of the signature */
  private static final String FIELDNAME = "sig";

  /** The source file */
  private final File sourceFile;

  /** The temporary file within signature, but without LTV enabled */
  private final File tempFileWithSignature;

  /** The temporary file within empty signature field */
  private final File tempFileEmptySignature;

  /** The destination file */
  private final File destinationFile;

  /** The destination file */
  private final File demo;

  /** The transaction id */
  private final Transaction transaction;

  /** The public certificate chain of the signature */
  private final Certificate[] chain;

  /** Is the document prepared */
  private boolean isPrepared;

  /** The document hash */
  private byte[] documentHash;

  /** The authenticated attributes of the document hash */
  private byte[] authenticatedAttributeBytes;

  /** The signed document hash */
  private byte[] signedHash;

  /** Is the signature correct or not */
  private boolean isSignatureCorrect;

  /** Is signature visible */
  private boolean visibleSignature;

  /**
   * Constructor.
   *
   * @param sourceFile The source file.
   * @param destinationLocation The destination location
   * @param chain The public certificate chain of the signature
   * @throws IOException If IO exception occurs
   */
  public SignDocument(
          File sourceFile,
          String destinationLocation,
          Certificate[] chain,
          String tempPdfLocation,
          Transaction transaction)
      throws IOException {
    this.sourceFile = sourceFile;
    tempFileEmptySignature = File.createTempFile("temp_", ".pdf", new File(tempPdfLocation));
    tempFileWithSignature = File.createTempFile("temp_", ".pdf", new File(tempPdfLocation));
    destinationFile = new File(destinationLocation + "\\signed_" + sourceFile.getName());
    demo = new File(destinationLocation + "\\demo_" + sourceFile.getName());
    this.chain = chain;
    isSignatureCorrect = false;
    isPrepared = false;
    visibleSignature = false;
    this.transaction = transaction;
  }

  /**
   * Returns if the signature is visible.
   *
   * @return is the signature visible
   */
  public boolean isVisibleSignature() {
    return visibleSignature;
  }

  /**
   * Sets the signature visible or not.
   *
   * @param visible The visibility of the signature
   */
  public void setVisibleSignature(boolean visible) {
    visibleSignature = visible;
  }

  /**
   * Returns if the sign document is prepared.
   *
   * @return is prepared
   */
  public boolean isPrepared() {
    return isPrepared;
  }

  /**
   * Sets the signed hash value.
   *
   * @param signedHash The signed hash
   */
  public void setSignedHash(byte[] signedHash) {
    this.signedHash = signedHash;
  }

  /**
   * Returns the authentically attributes bytes.
   *
   * @return byte []
   */
  public byte[] getAuthenticatedAttributeBytes() {
    return authenticatedAttributeBytes;
  }

  /**
   * Calculates the document hash.
   *
   * @return calculated document hash
   * @throws IOException If a IO exception occurs
   * @throws GeneralSecurityException If a general security error occurs
   */
  public String calculateDocumentHash() throws IOException, GeneralSecurityException {
    try (PdfReader pdfReader = new PdfReader(tempFileEmptySignature);
         PdfDocument document = new PdfDocument(pdfReader)) {
      IRandomAccessSource readerSource = document.getReader().getSafeFile().createSourceView();
      SignatureUtil signatureUtil = new SignatureUtil(document);
      PdfSignature signature = signatureUtil.getSignature(FIELDNAME);
      PdfArray b = signature.getByteRange();
      long[] gaps = b.toLongArray();
      try (InputStream rg =
          new RASInputStream((new RandomAccessSourceFactory()).createRanged(readerSource, gaps))) {

        final String hashAlgorithm = "SHA-256";
        BouncyCastleDigest digest = new BouncyCastleDigest();
        PdfPKCS7 pkcs7 = new PdfPKCS7(null, chain, hashAlgorithm, null, digest, false);
        documentHash = DigestAlgorithms.digest(rg, digest.getMessageDigest(hashAlgorithm));

        String url = CertificateUtil.getOCSPURL((X509Certificate) chain[0]);
        byte[] ocsp =
            new OcspClientBouncyCastle(new OCSPVerifier(null, null))
                .getEncoded((X509Certificate) chain[0], (X509Certificate) chain[1], url);

        authenticatedAttributeBytes =
            pkcs7.getAuthenticatedAttributeBytes(
                documentHash, ocsp, null, PdfSigner.CryptoStandard.CMS);
      }
    }

    return Base64.getEncoder().encodeToString(authenticatedAttributeBytes);
  }

  /**
   * Creates an empty sign field to the document.
   *
   * @throws IOException If a IO error occurs
   * @throws GeneralSecurityException If a general security error occurs
   */
  public void createEmptySignField() throws IOException, GeneralSecurityException {
    try (PdfReader reader = new PdfReader(sourceFile);
         FileOutputStream tempFileOutputStream = new FileOutputStream(tempFileEmptySignature)) {
      PdfSigner signer = new PdfSigner(reader, tempFileOutputStream, new StampingProperties());

      PdfSignatureAppearance appearance = signer.getSignatureAppearance();
      appearance
          .setReason(transaction.getReason())
          .setLocation(transaction.getLocation())
          .setPageNumber(1)
          .setCertificate(chain[0]);
      signer.setFieldName(FIELDNAME);

      /* ExternalBlankSignatureContainer constructor will create the PdfDictionary for the signature
       * information and will insert the /Filter and /SubFilter values into this dictionary.
       * It will leave just a blank placeholder for the signature that is to be inserted later.
       */
      IExternalSignatureContainer external =
          new ExternalBlankSignatureContainer(PdfName.Adobe_PPKLite, PdfName.Adbe_pkcs7_detached);

      // Sign the document using an external container.
      // 9800 is the size of the empty signature placeholder.
      signer.signExternalContainer(external, 12000);
    }
    isPrepared = true;
  }

  /**
   * Creates the signature in the document.
   *
   * @throws IOException If a IO error occurs
   */
  public void createSignature()
      throws IOException, GeneralSecurityException, OCSPException, OperatorException,
          StreamParsingException {
    try (PdfReader reader = new PdfReader(new FileInputStream(tempFileEmptySignature));
         FileOutputStream destinationOutputStream = new FileOutputStream(tempFileWithSignature)) {
      PdfSigner signer = new PdfSigner(reader, destinationOutputStream, new StampingProperties());

      IExternalSignatureContainer external = new PdfExternalSignatureContainer(chain, signedHash);
      try {
        signer.signDeferred(signer.getDocument(), FIELDNAME, destinationOutputStream, external);
        isSignatureCorrect = true;
      } catch (GeneralSecurityException ex) {
        logger.error("The signature of document [{}] is not valid.", destinationFile.getName());
        isSignatureCorrect = false;
      }
    }
    enableLTV();
    tempFileEmptySignature.delete();
  }

  /**
   * Enables the LTV for the given signature.
   *
   * @throws GeneralSecurityException If a security error occurs
   * @throws OCSPException If a OCSP error occurs
   * @throws IOException If an IO error occurs
   * @throws StreamParsingException If a stream parsing error occurs
   * @throws OperatorException If a operator error occurs
   */
  private void enableLTV()
      throws GeneralSecurityException, OCSPException, IOException, StreamParsingException,
          OperatorException {
    try (PdfReader pdfReader = new PdfReader(tempFileWithSignature);
         PdfWriter pdfWriter = new PdfWriter(destinationFile);
         PdfDocument pdfDocument =
            new PdfDocument(
                pdfReader,
                pdfWriter,
                new StampingProperties().preserveEncryption().useAppendMode())) {
      AdobeLtvEnabling adobeLtvEnabling = new AdobeLtvEnabling(pdfDocument);
      IOcspClient ocsp = new OcspClientBouncyCastle(null);
      ICrlClient crl = new CrlClientOnline();
      adobeLtvEnabling.enable(ocsp, crl);
    }
    tempFileWithSignature.delete();
  }

  /**
   * Gets the source file name.
   *
   * @return file name of the source file
   */
  public String getSourceFileName() {
    return sourceFile.getName();
  }

  /**
   * Gets the destination file name.
   *
   * @return file name of the destination file
   */
  public String getDestinationFileName() {
    return destinationFile.getName();
  }

  public boolean isSignatureValid() {
    return isSignatureCorrect;
  }

  /**
   * Gets the status if the signature is correct.
   *
   * @return signature correct status
   */
  public String isSignatureCorrect() {
    if (isSignatureCorrect) {
      return "YES";
    }
    return "NO";
  }
}
