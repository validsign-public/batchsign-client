package nl.validsign.client.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/** The TreeTableView item, to show all important information about the sign document. */
public class TreeTableViewDocument extends RecursiveTreeObject<TreeTableViewDocument> {

  /** The file name. */
  private final String name;
  /** List of errors */
  private final List<String> errors;
  /** The sign document. */
  private SignDocument signDocument;
  /** The document hash. */
  private String documentHash;
  /** Is signature visible. */
  private boolean isSignatureVisible;
  /** Is signature valid state. */
  private boolean validSignature;
  /** Is file prepared state. */
  private boolean prepared;

  /**
   * Constructor.
   *
   * @param signDocument The sign document.
   * @throws IOException If an IO error occurs.
   * @throws GeneralSecurityException If a security error occurs.
   */
  public TreeTableViewDocument(SignDocument signDocument, String... filename) {
    if (signDocument != null) {
      this.signDocument = signDocument;
      name = signDocument.getSourceFileName();
      documentHash =
          Base64.getEncoder().encodeToString(signDocument.getAuthenticatedAttributeBytes());
      prepared = signDocument.isPrepared();
      validSignature = signDocument.isSignatureValid();
      isSignatureVisible = signDocument.isVisibleSignature();
    } else {
      name = filename[0];
    }
    errors = new ArrayList<>();
  }

  /**
   * Constructor.
   *
   * @throws IOException If an IO error occurs.
   * @throws GeneralSecurityException If a security error occurs.
   */
  public TreeTableViewDocument(String filename, String documentHash) {
    signDocument = null;
    this.documentHash = documentHash;
    name = filename;
    prepared = false;
    validSignature = false;
    isSignatureVisible = false;

    errors = new ArrayList<>();
  }

  /**
   * Gets the sign document.
   *
   * @return sign document.
   */
  public SignDocument getSignDocument() {
    return signDocument;
  }

  /**
   * Gets the file name.
   *
   * @return name.
   */
  public String getName() {
    return name;
  }

  /**
   * Gets the sign document prepare state.
   *
   * @return prepare state.
   */
  public boolean isPrepared() {
    if (signDocument != null) {
      prepared = signDocument.isPrepared();
    }
    return prepared;
  }

  /**
   * Gets the sign document valid signatue state.
   *
   * @return valid signature state.
   */
  public boolean isSignatureValid() {
    if (signDocument != null) {
      validSignature = signDocument.isSignatureValid();
    }
    return validSignature;
  }

  /**
   * Returns if signature is visible or not.
   *
   * @return is signature visible
   */
  public boolean isSignatureVisible() {
    if (signDocument != null) {
      isSignatureVisible = signDocument.isVisibleSignature();
    }
    return isSignatureVisible;
  }

  /**
   * Sets the visibility of the signature.
   *
   * @param visible Is visible or not
   */
  public void setSignatureVisible(boolean visible) {
    if (signDocument != null) {
      signDocument.setVisibleSignature(visible);
    }
    isSignatureVisible();
  }

  /**
   * Gets the doucment hash.
   *
   * @return document hash.
   */
  public String getDocumentHash() {
    return documentHash;
  }
}
