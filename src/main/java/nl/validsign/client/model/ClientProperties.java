package nl.validsign.client.model;

import java.io.File;

/** The client properties object. */
public class ClientProperties {

  /** The API key. */
  private String apiKey;

  /** The output folder for signed documents. */
  private String outputFolder;

  /** The temporarily folder location for PDF's. */
  private final String tempPdfLocation;

  /** The public certificate file. */
  private final File publicCertificate;

  /**
   * Constructor.
   *
   * @param apiKey The API key
   * @param outputFolder The output folder
   * @param tempPdfLocation The temp PDF location
   * @param publicCertificate The public certificate
   */
  public ClientProperties(
      String apiKey,
      String outputFolder,
      String tempPdfLocation,
      File publicCertificate) {
    this.apiKey = apiKey;
    this.outputFolder = outputFolder;
    this.tempPdfLocation = tempPdfLocation;
    this.publicCertificate = publicCertificate;
  }

  /**
   * Gets the API key.
   *
   * @return API key
   */
  public String getApiKey() {
    return apiKey;
  }

  /**
   * Sets the API key.
   *
   * @param apiKey The API key
   */
  public void setApiKey(String apiKey) {
    this.apiKey = apiKey;
  }

  /**
   * Gets the output folder for signed documents.
   *
   * @return output folder
   */
  public String getOutputFolder() {
    return outputFolder;
  }

  /**
   * Set the output folder.
   *
   * @param outputFolder The output folder
   */
  public void setOutputFolder(String outputFolder) {
    this.outputFolder = outputFolder;
  }

  /**
   * Gets the temp PDF location.
   *
   * @return tempPdfLocation
   */
  public String getTempPdfLocation() {
    return tempPdfLocation;
  }

  /**
   * Gets the public certificate.
   *
   * @return publicCertificate
   */
  public File getPublicCertificate() {
    return publicCertificate;
  }
}
